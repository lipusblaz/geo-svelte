import App from './App.svelte';

const app = new App({
	target: document.body,
	props: {
		title: 'geo-svelte'
	}
});

export default app;