import { writable } from 'svelte/store';
import {createMap} from '../stores/mapStoreLogic';

let map = createMap()

export let olMap = writable(map);

export let olLayers = writable(map.getLayers().array_);