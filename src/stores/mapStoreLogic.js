import Map from 'ol/Map';
import {Tile as TileLayer} from 'ol/layer';
import {OSM} from 'ol/source';
import Stamen from 'ol/source/Stamen';
import View from 'ol/View';
import { fromLonLat, get } from 'ol/proj.js';

export const createMap = () => {
    let view = new View({
        center: fromLonLat([15, 46.2]),
        zoom: 9,
        projection: "EPSG:3857"
    });
    
    let ol = new TileLayer({
        source: new OSM(),
        title: 'OSM - Tiles',
    });
    let ol1 = new TileLayer({
        source: new Stamen({
            layer: "toner"
        }),
        title: 'Stamen toner Tiles',
    });
    let ol2 = new TileLayer({
        source: new Stamen({
            layer: "watercolor"
        }),
        title: 'Stamen watercolor Tiles',
    });
    let ol3 = new TileLayer({
        source: new Stamen({
            layer: "terrain"
        }),
        title: 'Stamen terrain Tiles',
    });

    return new Map({
        view: view,
        layers: [ol, ol1, ol2, ol3]
    });
}
